// import { module, test } from 'qunit';
// import { setupRenderingTest } from 'ember-qunit';
// import { render } from '@ember/test-helpers';
// import hbs from 'htmlbars-inline-precompile';

// module('Integration | Component | multi-step-page/buttons/cancel', function(hooks) {
//   setupRenderingTest(hooks);

//   test('it renders', async function(assert) {
//     // Set any properties with this.set('myProperty', 'value');
//     // Handle any actions with this.set('myAction', function(val) { ... });

//     await render(hbs`{{multi-step-page/buttons/cancel}}`);

//     assert.equal(this.element.textContent.trim(), '');

//     // Template block usage:
//     await render(hbs`
//       {{#multi-step-page/buttons/cancel}}
//         template block text
//       {{/multi-step-page/buttons/cancel}}
//     `);

//     assert.equal(this.element.textContent.trim(), 'template block text');
//   });
// });
