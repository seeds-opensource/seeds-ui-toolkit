import Helper from '@ember/component/helper';
import { htmlSafe } from '@ember/string';

export default Helper.extend({
  compute(param) {
    let css = param[0] || "";
    return htmlSafe(css);
  }
});
