import { get, set, computed } from '@ember/object';
import { readOnly } from '@ember/object/computed';

import { unixTimestampToDate } from './macros';

export const PROFILE_STATUS_VISITOR = 'visitor';
export const PROFILE_STATUS_RESIDENT = 'resident';
export const PROFILE_STATUS_CITIZEN = 'citizen';

export default class UserProfile {
  originalProfile;

  constructor(blockchainProfile) {
    set(this, 'originalProfile', blockchainProfile);
  }

  @readOnly('originalProfile.account') account;
  @readOnly('originalProfile.image') image;
  @readOnly('originalProfile.interests') interests;
  @readOnly('originalProfile.nickname') nickname;
  @readOnly('originalProfile.reputation') reputation;
  @readOnly('originalProfile.roles') roles;
  @readOnly('originalProfile.skills') skills;
  @readOnly('originalProfile.status') status;
  @readOnly('originalProfile.story') story;
  @readOnly('originalProfile.timestamp') timestamp;
  @readOnly('originalProfile.type') type;

  @unixTimestampToDate('timestamp') registrationDate;

  @computed('registrationDate')
  get accountAge() {
    const registrationDate = get(this, 'registrationDate');
    return new Date() - registrationDate;
  }

  @computed('accountAge')
  get accountAgeDays() {
    const accountAge = get(this, 'accountAge');
    return Math.floor(accountAge / (1000 * 24 * 60 * 60));
  }
}
