import { set } from '@ember/object';

import {
  unixTimestampToDate,
  moneyQuantity
} from 'seeds/utils/macros';

export default class ExchangeSettings {
  originalSettings;

  constructor(originalSettings) {
    set(this, 'originalSettings', originalSettings);
  }

  @moneyQuantity('originalSettings.rate') rate;
  @moneyQuantity('originalSettings.visitor_limit') visitorLimit;
  @moneyQuantity('originalSettings.resident_limit') residentLimit;
  @moneyQuantity('originalSettings.citizen_limit') citizenLimit;

  @unixTimestampToDate('originalSettings.timestamp') time;
}
