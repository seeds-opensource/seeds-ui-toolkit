import { get } from '@ember/object';

export default function arrayGroupBy(array, key, itemsKey = 'items') {
  const groups = [];
  for(const item of array) {
    const value = get(item, key);
    const group = groups.findBy(key, value);
    if(group) {
      group[itemsKey].push(item);
    } else {
      groups.push({
        [key]: value,
        [itemsKey]: [item]
      });
    }
  }
  return groups;
}
