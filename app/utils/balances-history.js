import {
  ACTION_REGISTER,
  ACTION_PLANT,
  ACTION_UNPLANT,
  ACTION_TRACK_CLAIM_REFUND,
  ACTION_TRACK_CANCEL_REFUND,
  ACTION_REWARD,
  ACTION_TRACK_CLAIM_REWARD,
  ACTION_TRANSFER,
  ACTION_ISSUE,
  ACTION_INVITE_USER,
  ACTION_INVITED_BY_USER
} from './user-action';

export default class BalancesHistory {
  currentAccountName;

  available = 0;
  planted = 0;
  unplanting = 0;
  harvest = 0;

  snapshots = [];

  constructor(currentAccountName, available = 0, planted = 0, unplanting = 0, harvest = 0, actionGroups = []) {
    this.currentAccountName = currentAccountName;
    this.available = available;
    this.planted = planted;
    this.unplanting = unplanting;
    this.harvest = harvest;
    this.resetHarvest();
    let stopProcessingActions = false;
    for(const actionGroup of actionGroups) {
      this.createGroupSnapshot(actionGroup);
      this.resetHarvest();
      if(!stopProcessingActions) {
        if(!this.processGroupActions(actionGroup)) {
          stopProcessingActions = true;
        }
      }
    }
  }

  resetHarvest() {
    this.harvest = 0; // we show only claimrewards during period
  }

  createGroupSnapshot({ date }) {
    this.snapshots.addObject({
      date,
      available: this.available,
      planted: this.planted,
      unplanting: this.unplanting,
      harvest: this.harvest,
      sum: this.available + this.planted + this.unplanting + this.harvest
    });
  }

  processGroupActions({ actions = [] }) { // eslint-disable-line complexity
    for(const { name, quantity, to } of actions) {
      switch(name) {
      case ACTION_REGISTER:
        this.planted = 0;
        this.available = 0;
        this.unplanting = 0;
        this.harvest = 0;
        return false;
      case ACTION_PLANT:
        this.planted -= quantity;
        this.available += quantity;
        break;
      case ACTION_UNPLANT:
        this.planted += quantity;
        this.unplanting -= quantity;
        break;
      case ACTION_TRACK_CLAIM_REFUND:
        this.unplanting += quantity;
        this.available -= quantity;
        break;
      case ACTION_TRACK_CANCEL_REFUND:
        this.unplanting += quantity;
        this.planted -= quantity;
        break;
      case ACTION_REWARD:
        this.harvest -= quantity;
        break;
      case ACTION_TRACK_CLAIM_REWARD:
        this.harvest += quantity;
        this.available -= quantity;
        break;
      case ACTION_INVITE_USER:
        this.available += quantity;
        break;
      case ACTION_INVITED_BY_USER:
        this.available -= quantity;
        break;
      case ACTION_TRANSFER:
        if(to === this.currentAccountName) {
          this.available -= quantity;
        } else {
          this.available += quantity;
        }
        break;
      case ACTION_ISSUE:
        this.available -= quantity;
        break;
      }
    }
    return true;
  }
}
