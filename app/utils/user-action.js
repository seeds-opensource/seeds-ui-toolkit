import { get, getProperties, setProperties, computed } from '@ember/object';
import { readOnly, equal, notEmpty } from '@ember/object/computed';
import moment from 'moment';

import { moneyCurrency, moneyQuantity } from './macros';

export const ACTION_PLANT = 'plant';
export const ACTION_UNPLANT = 'unplant';
export const ACTION_CLAIM_REFUND = 'claimrefund';
export const ACTION_CANCEL_REFUND = 'cancelrefund';
export const ACTION_REWARD = 'reward'; // harvest
export const ACTION_CLAIM_REWARD = 'claimreward'; // withdraw, gather
export const ACTION_TRANSFER = 'transfer';
export const ACTION_ISSUE = 'issue';
export const ACTION_INVITE_USER = 'inviteuser';
export const ACTION_INVITED_BY_USER = 'invitedbyuser';
export const ACTION_VOUCH = 'vouch';
export const ACTION_REGISTER = 'register';
export const ACTION_MAKE_RESIDENT = 'makeresident';
export const ACTION_MAKE_CITIZEN = 'makecitizen';
export const ACTION_DEPRECATED = 'deprecated';

export const ACTION_TRACK_CLAIM_REWARD = 'trackreward';
export const ACTION_TRACK_CLAIM_REFUND = 'trackrefund';
export const ACTION_TRACK_CANCEL_REFUND = 'trackcancel';

export const SPECIAL_TRANSFER_PLANT = ACTION_PLANT;
export const SPECIAL_TRANSFER_INVITE_USER = ACTION_INVITE_USER;
export const SPECIAL_TRANSFER_INVITED_BY_USER = ACTION_INVITED_BY_USER;
export const SPECIAL_TRANSFER_DEPRECATED = ACTION_DEPRECATED;

export default class UserAction {
  specialAccounts;
  originalAction;

  constructor(specialAccounts, originalAction) {
    setProperties(this, {
      specialAccounts,
      originalAction
    });
  }

  @readOnly('originalAction.action_trace') originalActionTrace;
  @readOnly('originalActionTrace.act') originalActionAct;
  @readOnly('originalActionTrace.block_time') originalActionTime;
  @readOnly('originalActionAct.data') originalActionData;
  @readOnly('originalActionData.quantity') originalQuantity;

  @readOnly('originalActionTrace.trx_id') trxId;

  @readOnly('originalActionAct.name') name;

  @readOnly('originalActionData.from') from;
  @readOnly('originalActionData.to') to;
  @readOnly('originalActionData.account') account;

  @moneyQuantity('originalQuantity') quantity;
  @moneyCurrency('originalQuantity') currency;

  @computed('originalActionTime')
  get time() {
    const originalActionTime = get(this, 'originalActionTime');
    return moment.utc(originalActionTime).toDate();
  }

  @computed('name', 'trxId')
  get uniqueId() {
    const {
      name,
      trxId
    } = getProperties(this,
      'name',
      'trxId'
    );
    return `${name}-${trxId}`;
  }

  @equal('name', ACTION_TRANSFER) isTransfer;

  @computed('specialAccounts', 'isTransfer', 'to', 'from')
  get specialTransferType(){
    const {
      specialAccounts,
      isTransfer,
      to,
      from
    } = getProperties(this,
      'specialAccounts',
      'isTransfer',
      'to',
      'from'
    );
    if(!isTransfer) return undefined;
    switch(to) {
      case specialAccounts.HARVEST:
        return SPECIAL_TRANSFER_PLANT;
      case specialAccounts.ESCROW:
        return SPECIAL_TRANSFER_INVITE_USER;
    }
    switch(from) {
      case specialAccounts.ESCROW:
        return SPECIAL_TRANSFER_INVITED_BY_USER;
      case specialAccounts.BANK:
        return SPECIAL_TRANSFER_DEPRECATED;
    }
    return undefined;
  }

  @notEmpty('specialTransferType') isSpecialTransfer;
}
