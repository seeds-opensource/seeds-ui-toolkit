export default function arraySumBy(array, key, initialValue = 0) {
  return array.reduce(function(sum, { [key]: value }) {
    return sum + value;
  }, initialValue);
}

