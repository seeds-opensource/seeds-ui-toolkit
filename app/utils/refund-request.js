import { readOnly, filterBy } from '@ember/object/computed';
import { sumBy } from './macros';

export default class RefundRequest {
  requestId;
  refunds = [];

  constructor(requestId, refunds = []) {
    this.requestId = requestId;
    this.refunds = refunds;
  }

  @filterBy('refunds', 'isClaimable', true) claimableRefunds;
  @filterBy('refunds', 'isClaimable', false) unclaimableRefunds;

  @sumBy('refunds', 'quantity') totalQuantity;
  @sumBy('claimableRefunds', 'quantity') claimableQuantity;
  @sumBy('unclaimableRefunds', 'quantity') unclaimableQuantity;

  @readOnly('refunds.firstObject') firstRefund;
  @readOnly('refunds.lastObject') lastRefund;
  @readOnly('unclaimableRefunds.firstObject') nextRefund;

  @readOnly('firstRefund.requestTime') requestTime;
  @readOnly('lastRefund.refundTime') refundTime;
}
