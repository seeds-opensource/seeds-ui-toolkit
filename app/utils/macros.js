import { get, getProperties, computed } from '@ember/object';
import { filter } from '@ember/object/computed';

import { parseMoney, DEFAULT_CURRENCY } from './currency';
import arrayGroupBy from './array-group-by';
import arraySumBy from './array-sum-by';

//#region Miscellaneous

export function unixTimestampToDate(propertyName) {
  return computed(propertyName, function() {
    const value = get(this, propertyName);
    return typeof value === 'number' ? new Date(value * 1000) : undefined;
  });
}

//#endregion

//#region String Manipulation

export function upperCase(propertyName) {
  return computed(propertyName, function() {
    const value = get(this, propertyName) || '';
    return String(value).toUpperCase();
  });
}

export function lowerCase(propertyName) {
  return computed(propertyName, function() {
    const value = get(this, propertyName) || '';
    return String(value).toLowerCase();
  });
}

//#endregion

//#region Parsing User Input

export function parsedUserDecimal(propertyName) {
  return computed(propertyName, function() {
    const value = get(this, propertyName) || '0';
    return value.replace(',', '.');
  });
}

export function parsedUserMoney(propertyName) {
  return computed(propertyName, function() {
    const value = get(this, propertyName) || '0';
    const parsedValue = parseFloat(value).toFixed(4);
    return `${parsedValue} ${DEFAULT_CURRENCY}`;
  });
}

//#endregion

//#region Parsing currency

export function moneyQuantity(propertyName) {
  return computed(propertyName, function() {
    const quantityWithCurrency = get(this, propertyName);
    const { quantity } = parseMoney(quantityWithCurrency);
    return quantity;
  });
}

export function moneyCurrency(propertyName) {
  return computed(propertyName, function() {
    const quantityWithCurrency = get(this, propertyName);
    const { currency } = parseMoney(quantityWithCurrency);
    return currency;
  });
}

//#endregion

//#region Mathematical

export function add(propertyName, increment = 0) {
  return computed(propertyName, function() {
    const value = get(this, propertyName);
    return value + increment;
  });
}

export function subtract(propertyName, decrement = 0) {
  return computed(propertyName, function() {
    const value = get(this, propertyName);
    return value - decrement;
  });
}

export function multiply(propertyName, multiplier = 0) {
  return computed(propertyName, function() {
    const value = get(this, propertyName);
    return value * multiplier;
  });
}

export function divide(propertyName, divisor = 0) {
  return computed(propertyName, function() {
    const value = get(this, propertyName);
    return value / divisor;
  });
}

export function addProperties(propertyA, propertyB) {
  return computed(propertyA, propertyB, function() {
    const {
      [propertyA]: valueA = 0,
      [propertyB]: valueB = 0
    } = getProperties(this, propertyA, propertyB);
    return valueA + valueB;
  });
}

export function subtractProperties(propertyA, propertyB) {
  return computed(propertyA, propertyB, function() {
    const {
      [propertyA]: valueA = 0,
      [propertyB]: valueB = 0
    } = getProperties(this, propertyA, propertyB);
    return valueA - valueB;
  });
}

export function multiplyProperties(propertyA, propertyB) {
  return computed(propertyA, propertyB, function() {
    const {
      [propertyA]: valueA = 0,
      [propertyB]: valueB = 0
    } = getProperties(this, propertyA, propertyB);
    return valueA * valueB;
  });
}

export function divideProperties(propertyA, propertyB) {
  return computed(propertyA, propertyB, function() {
    const {
      [propertyA]: valueA = 0,
      [propertyB]: valueB = 0
    } = getProperties(this, propertyA, propertyB);
    return valueA / valueB;
  });
}

//#endregion

//#region Property comparison

export function equalProperty(a, b) {
  return computed(a, b, function() {
    return get(this, a) === get(this, b);
  });
}

export function gtProperty(a, b) {
  return computed(a, b, function() {
    return get(this, a) > get(this, b);
  });
}

export function gteProperty(a, b) {
  return computed(a, b, function() {
    return get(this, a) >= get(this, b);
  });
}

export function ltProperty(a, b) {
  return computed(a, b, function() {
    return get(this, a) < get(this, b);
  });
}

export function lteProperty(a, b) {
  return computed(a, b, function() {
    return get(this, a) <= get(this, b);
  });
}

export function inArray(property, array) {
  return computed(property, function() {
    const value = get(this, property);
    return array.includes(value);
  });
}

//#endregion

//#region Switching

export function ternary(property, ifTrue, ifFalse) {
  return computed(property, function() {
    const value = get(this, property);
    return value ? ifTrue : ifFalse;
  });
}

export function switchBy(keyProperty, valuesMap, defaultValue) {
  return computed(keyProperty, function() {
    const key = get(this, keyProperty);
    if(valuesMap.hasOwnProperty(key)) {
      return get(valuesMap, key);
    } else {
      return defaultValue;
    }
  });
}

//#endregion

//#region Filtering

export function filterByInArray(property, key, array) {
  return filter(property, [`${property}.@each.${key}`], function(member) {
    const value = get(member, key);
    return array.includes(value);
  });
}

export function filterByProperty(property, key, anotherProperty) {
  const dependencies = [
    `${property}.@each.${key}`,
    anotherProperty
  ];
  return filter(property, dependencies, function(member) {
    const anotherPropertyValue = get(this, anotherProperty);
    const value = get(member, key);
    return value === anotherPropertyValue;
  });
}

//#endregion

//#region Grouping

export function groupBy(arrayProperty, key, itemsKey) {
  return computed(`${arrayProperty}.[]`, function(){
    const array = get(this, arrayProperty);
    return arrayGroupBy(array, key, itemsKey);
  });
}

export function sumBy(arrayProperty, key, initialValue) {
  return computed(`${arrayProperty}.[]`, function(){
    const array = get(this, arrayProperty);
    return arraySumBy(array, key, initialValue);
  });
}

//#endregion
