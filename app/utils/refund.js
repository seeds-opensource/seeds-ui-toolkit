import { get, getProperties, computed } from '@ember/object';
import { readOnly } from '@ember/object/computed';
import moment from 'moment';

import {
  unixTimestampToDate,
  moneyQuantity,
  moneyCurrency
} from 'seeds/utils/macros';

export default class Refund {
  originalRefund;

  constructor(originalRefund) {
    this.originalRefund = originalRefund;
  }

  @readOnly('originalRefund.request_id') requestId;
  @readOnly('originalRefund.refund_id') refundId;
  @readOnly('originalRefund.account') account;
  @readOnly('originalRefund.weeks_delay') weeksDelay;

  @moneyQuantity('originalRefund.amount') quantity;
  @moneyCurrency('originalRefund.amount') currency;

  @unixTimestampToDate('originalRefund.request_time') requestTime;

  @computed('requestTime', 'weeksDelay')
  get refundTime() {
    const {
      requestTime,
      weeksDelay
    } = getProperties(this,
      'requestTime',
      'weeksDelay'
    );
    return moment(requestTime).add(weeksDelay, 'weeks').toDate();
  }

  @computed('refundTime')
  get isClaimable() {
    const refundTime = get(this, 'refundTime');
    return moment().isSameOrAfter(refundTime);
  }
}
