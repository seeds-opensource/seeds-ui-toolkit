export const CURRENCY_SEEDS = 'SEEDS';
export const CURRENCY_TLOS = 'TLOS'

export const DEFAULT_CURRENCY = CURRENCY_SEEDS;

export function parseMoney(quantityWithCurrency = `0 ${DEFAULT_CURRENCY}`) {
  return {
    quantity: parseFloat(quantityWithCurrency.replace(/\s.*$/, ''), 10),
    currency: quantityWithCurrency.replace(/^\d+(\.\d+)?\s/, '')
  };
}
