import { set } from '@ember/object';
import { map } from '@ember/object/computed';

import RefundRequest from './refund-request';
import { groupBy, sumBy, add } from './macros';
import { DEFAULT_CURRENCY } from './currency';

export default class RefundsTable {
  refunds;

  constructor(refunds) {
    set(this, 'refunds', refunds);
  }

  @groupBy('refunds', 'requestId', 'refunds') groupedRefunds;

  @map('groupedRefunds', ['groupedRefunds.@each.{requestId,refunds}'], function({requestId, refunds}) {
    return new RefundRequest(requestId, refunds);
  }) refundRequests;

  @sumBy('refundRequests', 'totalQuantity') refundsSum;

  @add('refundsSum', ` ${DEFAULT_CURRENCY}`) unplantingBalance;
}
