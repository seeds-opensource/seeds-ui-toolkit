/* eslint-disable */
import Component from '@ember/component';
import { task } from 'ember-concurrency'

export default class LoadingButton extends Component {

  @(task(function*(ev) {
    ev.preventDefault();
    yield this.clickAction()
  }).drop()) runAction;

}