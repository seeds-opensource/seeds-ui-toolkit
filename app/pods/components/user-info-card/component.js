import Component from '@ember/component';
import { get, set, computed, setProperties } from '@ember/object';
import { inject as service } from '@ember/service';
import _ from 'lodash';
import config from 'seeds/config/environment';
import { htmlSafe } from '@ember/template';

export default class UserInfoCardComponent extends Component.extend() {
  @service peopleManager;
  @service loadingOverlay;

  rootURL    = config.rootURL;
  classNames = ['column'];


  @computed('type')
  get contributionLabel(){
    let account = get(this, 'account') || "";
    return account === 'organisation' ? 'Contribution' : 'Community';
  }

  @computed('type')
  get reputationLabel(){
    let reputation = get(this, 'reputation') || "";
    return reputation === 'organisation' ? 'Community' : 'Reputation';
  }

  @computed('type')
  get plantedLabel(){
    let planted = get(this, 'planted') || "";
    return planted === 'organisation' ? 'Biosphere' : 'Planted';
  }

  @computed('type')
  get transactionLabel(){
    let transaction = get(this, 'transaction') || "";
    return transaction === 'organisation' ? 'Economy' : 'Transaction';
  }

  @computed('profileImage')
  get bgProfileImage() {
    const file = get(this, 'profileImage');
    return htmlSafe(`background-image: url("${file}");`);
  }

  _updateUserData( userInfo ){
    
    // Added so that any change in API structure
    // Don't affect the app

    let allowedKeys = {
      'account'   : 'account',
      'image'     : 'profileImage',
      'interests' : 'interests',
      'nickname'  : 'nickname',
      'roles'     : 'roles',
      'skills'    : 'skills',
      'story'     : 'story',

      'likes'      : 'likes',
      'dislikes'   : 'dislikes',
      'comments'   : 'comments',
      'edits'      : 'edits',
      'shared'     : 'shared',

      'contribution_score' : 'contribution',
      'planted_score'      : 'planted',
      'reputation_score'   : 'reputation',
      'transactions_score' : 'transaction',

    };


    _.forOwn(userInfo, (value, key) => { 
      if( allowedKeys[key]){
        set(this, allowedKeys[key] , value);
      }
    });

  }


  showOverLay(){
    get(this, 'loadingOverlay').startLoading(); 
  }
  hideOverLay(){
    get(this, 'loadingOverlay').endLoading();
  }

  didReceiveAttrs(){
    this.initScreenInfo()
    this.getScreenInfo();
  }
   
  // Reset the variable configuration
  initScreenInfo(){
    setProperties(this, {
      people: [],
      account: undefined,
      profileImage: undefined,
      interests: [],
      nickname: undefined,
      roles: [] ,
      skills: [],
      story: undefined,
      likes: undefined,
      dislikes: undefined,
      comments: undefined,
      edits: undefined,
      shared: undefined,
      contribution: undefined,
      planted: undefined,
      reputation: undefined,
      transaction: undefined
    });

  }

  //
  // Fetch Person details
  async getScreenInfo(){
    try{
      this.showOverLay();

      let userdata= get(this, 'userdata');

      if( userdata ){
        return this._updateUserData(userdata);
      }

      let accountName = get(this, 'accountName');
      let pM   = get(this, 'peopleManager');

      //TODO: Correct this to fetch single user details
      // This function is also getting called during user change
      // So cache need to be implemented accordingly
      let info = await pM.getAllPeopleAndScore( accountName , true );
      return this._updateUserData(info);

    }catch(e){
      // TODO: Improve error handling
      alert('Error while getting profile Information');
    }finally{
      this.hideOverLay();
    }

  }


}
