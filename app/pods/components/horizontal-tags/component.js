import Component from '@ember/component';
import { get, computed } from '@ember/object';

export default class HorizontalTagComponent extends Component.extend() {

  maxiumTag = 3;
  tags      = [];

  defaultColumns = 3;  // Is array is having less then 3 element, add empty tags


  @computed('tags')
  get getTagsNestedArray(){
    let nArray = []; // Nested Array
    let tags   = get(this, 'tags') || []; 
    let max    = parseInt( get(this, 'maxiumTag') );

    tags.forEach( (tag) => {
      if( nArray.length === 0 || nArray[ nArray.length - 1  ].length === max ){
        nArray.push([]);
      }
      nArray[ nArray.length - 1 ].push(tag);
    });

    // Adding empty tags at end, this will add columns
    nArray.forEach( (tags) => {
      if(tags.length < max){
        for (let step = tags.length; step < max; step++) {
          tags.push("");
        }
      }
    });

    return nArray;
  }
}
