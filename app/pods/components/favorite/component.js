import Component from '@ember/component';
import { get } from '@ember/object';
import ENV from 'seeds/config/environment';

export default class FavoriteComponent extends Component {

  classNames = ['favorite']

  classNameBindings = ['active:active:not-active']

  rootURL = ENV.rootURL

  active = false

  click() {
    const callback = get(this, 'on-change');
    callback && callback(!get(this, 'active'));
  }

}
