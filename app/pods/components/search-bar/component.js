import Component from '@ember/component';
import { get, set, action } from '@ember/object';
import { next, debounce }  from '@ember/runloop';
import $ from 'jquery';

export default class SearchBarComponent extends Component {

  classNames = ['search-bar-container']
  classNameBindings = ['isOpen:open']

  value = ''
  isOpen = false
  hideOnClickOutside = true
  onChangeInterval = 500 // miliseconds
  placeholder = 'Search'

  constructor() {
    super(...arguments);
    this.setupOutsideClick();
  }

  /**
   * Hide the input when clicked outside the component
   */
  setupOutsideClick() {
    $(document).click(function(event) {
      if (!get(this, 'hideOnClickOutside')) {
        return;
      }

      const $target = $(event.target);

      if (!$target.closest(`#${this.elementId}`).length) {
        if (!get(this, 'isDestroying') && !get(this, 'isDestroying')) {
          set(this, 'isOpen', false);
        }
      }
    }.bind(this));
  }

  /**
   * Show/hide the search bar
   * When it's open the input's value is selected to help the user update it
   */
  @action
  toggle() {
    const isOpen = this.toggleProperty('isOpen');

    next(() => {
      isOpen && $(`#${this.elementId} input`).select();
    });

    event && event.stopPropagation();
  }

  /**
   * When the user is typing the component wait until trigger the callback
   * When the user press enter the component immediately trigger the callback 
   */
  @action
  onChange() {
    const debounceInterval = event.keyCode === 13 ? 0 :this.onChangeInterval;
    get(this, 'on-change') && debounce(this, this._onChange, debounceInterval);
  }

  _onChange() {
    get(this, 'on-change')(get(this, 'value'));
  }

}
