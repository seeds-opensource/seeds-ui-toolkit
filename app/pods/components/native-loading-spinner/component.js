import Component from '@ember/component';
import { computed } from '@ember/object';

export default class NativeLoadingSpinner extends Component {

  @computed('')
  get device() {
    if (window.cordova && window.device.platform === 'iOS') {
      return 'ios';
    } else {
      return 'android';
    }
  }

} 