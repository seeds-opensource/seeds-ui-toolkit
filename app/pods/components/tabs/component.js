/**
 * https://bulma.io/documentation/components/tabs/
 */

import Component from '@ember/component';
import { get, action }  from '@ember/object';

export default class TabsComponent extends Component {

  classNames = ['tabs']
  classNameBindings = ['isToggle:is-toggle:']

  isToggle = true
  labelField = null
  valueField = null

  @action
  onSelect(tab) {
    const callback = get(this, 'on-select');
    callback && callback(tab);
  }

}
