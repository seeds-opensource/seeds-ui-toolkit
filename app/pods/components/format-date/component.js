import Component from '@ember/component';
import { getProperties, computed } from '@ember/object';
import moment from 'moment';

export default class FormatDateComponent extends Component {
  classNames = ['format-date'];

  value;
  parseFormat;
  displayFormat;

  @computed('value', 'parseFormat', 'displayFormat')
  get formattedValue() {
    const {
      value,
      parseFormat,
      displayFormat
    } = getProperties(this,
      'value',
      'parseFormat',
      'displayFormat'
    );
    return moment(value, parseFormat).format(displayFormat);
  }
}
