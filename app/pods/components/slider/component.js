/* eslint ember/no-incorrect-calls-with-inline-anonymous-functions: off */
import Component from '@ember/component';
import Glide from '@glidejs/glide';
import { run } from '@ember/runloop';
import { computed } from '@ember/object';

export default class SliderComponent extends Component {
  bullets = 0;

  didInsertElement() {
    super.didInsertElement(...arguments);
    run.scheduleOnce('afterRender', () => {
      this._mountSlider();
    });
  }

  /**
   * Create an array of bullets ID
   */
  @computed('bullets')
  get bulletsArr() {
    return Array.from({ length: this.bullets }, (_, i) => `=${i}`);
  }

  /**
   * Initialize and mount the slider
   */
  _mountSlider() {
    new Glide('.glide', {
      autoplay: 4000,
      animationDuration: 1000
    }).mount()
  }
}
