import Component from '@ember/component';
import { action, get, set, computed } from '@ember/object';

export default class MultiStepPageButtonNextComponent extends Component {
  beforeNext;
  label = 'NEXT';
  isStickButton = false;

  goTo(nextStep) {
    set(this, 'msp.direction', 'next');
    get(this, 'msp.goTo')(nextStep);
  }

  @computed('isStickButton')
  get buttonClasses() {
    if (this.isStickButton) {
      return 'button is-large is-uppercase is-fullwidth is-stick';
    } else {
      return 'button is-white'
    }
  }

  @action
  async goToStep(nextStep) {
    const beforeNext = get(this, 'beforeNext');
    if (beforeNext) {
      try {
        await beforeNext();
      } catch {
        return;
      }
    }
    this.goTo(nextStep);
  }
}
