import Component from '@ember/component';
import { get, action } from '@ember/object';

export default class MultiStepPageButtonCancelComponent extends Component {
  beforeCancel;
  enabled = true;
  label = 'CANCEL';

  close() {
    const close = get(this, 'msp.close') || function(){};
    close();
  }

  @action
  async cancel() {
    const beforeCancel = get(this, 'beforeCancel');
    if (beforeCancel) {
      try {
        await beforeCancel();
      } catch {
        return;
      }
    }
    this.close();
  }
}
