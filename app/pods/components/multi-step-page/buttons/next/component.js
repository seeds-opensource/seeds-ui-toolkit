import Component from '@ember/component';
import { action, get, set } from '@ember/object';

export default class MultiStepPageButtonNextComponent extends Component {
  beforeNext;
  label = 'NEXT';
  isRunning = false;
  isStickButton = false;

  goTo(nextStep) {
    set(this, 'msp.direction', 'next');
    get(this, 'msp.goTo')(nextStep);
  }

  @action
  async goToStep(nextStep) {
    set(this, 'isRunning', true);
    const beforeNext = get(this, 'beforeNext');
    if (beforeNext) {
      try {
        await beforeNext();
      } catch {
        set(this, 'isRunning', false);
        return;
      }
    }
    set(this, 'isRunning', false);
    this.goTo(nextStep);
  }
}
