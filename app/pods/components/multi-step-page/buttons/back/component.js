import Component from '@ember/component';
import { action, get } from '@ember/object';

export default class MultiStepPageButtonBackComponent extends Component {
  label = 'BACK';

  @action
  goToPreviousStep() {
    const msp = get(this, 'msp');
    msp.goToPreviousStep();
  }
}
