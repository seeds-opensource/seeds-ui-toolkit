import Component from '@ember/component';
import { get, set, action, computed } from '@ember/object';

export default class MultiStepPageComponent extends Component {
  currentStep = 1;
  stepCount = 1;
  stepTitles = {};
  stepActions = {};

  enableBackButton = true;

  // Previous step map can either be false or an object. The key represents
  // the current step and the value represents the screen number to go
  // back to from that screen. Example: {3: 1, 5:2}
  previousStepMap = false;

  @computed('currentStep', 'previousStepMap.[')
  get backButtonDisabledInMap() {
    return this.previousStepMap && this.previousStepMap[this.currentStep] === false
  }

  @computed('currentStep', 'stepTitles.[]')
  get title() {
    return get(this, `stepTitles.${get(this, 'currentStep')}`);
  }

  registerStep(step, title, actions) {
    set(this, `stepActions.${step}`, actions);
    set(this, `stepTitles.${step}`, title);
    this.notifyPropertyChange('currentStep');
  }

  @action
  goToPreviousStep() {
    set(this, 'direction', 'previous');
    if (this.previousStepMap && this.previousStepMap[this.currentStep]) {
      this._goTo(this.previousStepMap[this.currentStep]);
    } else {
      this._goTo(get(this, 'currentStep') - 1);
    }
  }

  @action
  goToNextStep() {
    set(this, 'direction', 'next');
    this._goTo(get(this, 'currentStep') + 1);
  }

  /**
   * Go to a step
   * @param {*} step 
   * > number: the step to show
   * > action: a method to call
   */
  @action
  goTo(step) {
    if (typeof step === 'function') {
      return step();
    }

    this._goTo(step);
  }

  async _goTo(step) {
    const { beforeLoadStep } = get(this, `stepActions.${step}`) || {};
    if (beforeLoadStep) {
      try {
        await beforeLoadStep();
      } catch {
        return;
      }
    }
    set(this, 'currentStep', step);
  }
}
