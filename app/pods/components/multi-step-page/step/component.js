/* eslint ember/no-incorrect-calls-with-inline-anonymous-functions: off */
import Component from '@ember/component';
import { run } from '@ember/runloop';
import { get } from '@ember/object';

export default class MultiStepPageStepComponent extends Component {

  constructor() {
    super(...arguments);

    run.scheduleOnce('afterRender', ()=> {
      get(this, 'msp').registerStep(
        get(this, 'step'), 
        get(this, 'title'),
        {
          beforeLoadStep: get(this, 'beforeLoadStep')
        }
      );
    });
  }

}
