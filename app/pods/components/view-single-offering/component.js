import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { action, get, set, computed } from '@ember/object';
import { htmlSafe } from '@ember/template';

export default class ViewSingleOfferingComponent extends Component.extend() {

  @service loadingOverlay;
  @service peopleManager;
  @service ajaxPassport;

  @action
  close() {
    window.history.back();
  }
  
  @computed('model.banner')
  get bannerImage() {
    return this.getImage('banner');
  }

  @computed('model.icon')
  get iconImage() {
    return this.getImage('icon');
  }

  getImage(type){
    const file = get(this, `model.${type}`);
    return file && file['480'] ?  htmlSafe(`background-image: url("${file[480]}");`) : "";
  }

  @computed('model.terms_url')
  get termURL() {
    return this.getURL('terms_url');
  }

  @computed('model.support_url')
  get supportURL() {
    return this.getURL('support_url');
  }

  getURL(type){
    let url = get(this, `model.${type}`);
    return url.indexOf("http") === 0 ? url : `http://${url}`;
  }

  showOverLay(){
    get(this, 'loadingOverlay').startLoading(); 
  }

  hideOverLay(){
    get(this, 'loadingOverlay').endLoading();
  }

  didReceiveAttrs(){
    this.getScreenInfo();
  }
   
  async getScreenInfo() {
    set(this, 'state', 'init');
    try{
      this.showOverLay();
      let pid   = get(this, 'pid');
      let info  = await this.ajaxPassport.request(`/products/${pid}`);

      if( info && info.data && info.data.account_name ){
        let pM    = get(this, 'peopleManager');
        let userInfo = await pM.getAllPeopleAndScore( info.data.account_name , true );
        info.data['userInfo'] = userInfo;
        set(this, 'model', info.data);
        set(this, 'state', 'finished');
        return;
      }
      throw "Product information is incorrect";
    } catch{
      set(this, 'state', 'error');
      alert('Error while fetching product Information.');
    } finally{
      this.hideOverLay();
    }
  }

}
