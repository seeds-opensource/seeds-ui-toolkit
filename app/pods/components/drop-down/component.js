import Component from '@ember/component';
import { get, set, computed, action } from '@ember/object';
import { isPresent, isBlank } from '@ember/utils';

export default class DropDownComponent extends Component {

  classNames = ['drop-down-container']
  
  options = []
  optionValuePath = 'content.id'
  optionLabelPath = 'content.label'
  selected = undefined

  /**
   * Errors can be a array or a string
   * The array values can be a string or an object containing 'message' attribute
   * We treat the received value to always result in a array of objects
   * 
   * errors='Test error'
   * Result: [{ message: 'Test error' }]
   * 
   * errors=['test 1', 'test 2', 'test 3']
   * Result: [{ message: 'test 1' }, { message: 'test 2' }, { message: 'test 3' }]
   * 
   * errors=[{ title: 'title 1', message: 'test 1' }, { title: 'title 2', message: 'test 2' }, { title: 'title 3', message: 'test 3' }]
   * Result: [{ message: 'test 1' }, { message: 'test 2' }, { message: 'test 3' }]
   */
  @computed('errors', 'errors.@each.message')
  get errorsList() {
    const errors = get(this, 'errors');

    if (isBlank(errors)) {
      return [];
    }

    return [].concat(errors).map(error => {
      if (typeof error === 'string') {
        return { message: error };
      }
      return { message: get(error, 'message') };
    });
  }

  /**
   * Trigerred when the dropdown's value is changed
   * The 'selected' property must always be muted or dropdown won't show the option as selected
   */
  @action
  onChange(value) {
    if (!this._triggerCallback('on-change', arguments)) {
      set(this, 'selected', value);
    }
  }

  _triggerCallback(name, params) {
    const callback = get(this, name);
    callback && callback(...params);

    return isPresent(callback);
  }

}
