// TODO: Refactor. Importing the previous component doesn't make much sense.
import Component from '../component';
import {computed, action} from '@ember/object';
import {isPresent} from '@ember/utils';

export default class FileManagerModal extends Component {

  classNames = ['modal', 'is-active', 'file-manager-modal'];

  @computed('')
  get isIOS() {
    return isPresent(window.cordova) && device && device.platform === 'iOS'; // eslint-disable-line no-undef
  }

  didInsertElement() {
    this._loadFiles();
  }

  @action
  mobileUpload() {
    const cameraOptions = {
      quality: 50,
      sourceType: navigator.camera.PictureSourceType['PHOTOLIBRARY'],
      destinationType: navigator.camera.DestinationType.DATA_URL,
      encodingType: navigator.camera.EncodingType.JPEG,
      mediaType: navigator.camera.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: true
    };
    const cameraSuccess = file => {
      this.uploadImageMobile.call(this, file);
    };
    const cameraError = () => {};
    navigator.camera.getPicture(cameraSuccess, cameraError, cameraOptions);
  }


}
