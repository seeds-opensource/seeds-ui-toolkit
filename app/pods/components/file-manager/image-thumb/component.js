import Component from '@ember/component';
import { get, set, computed, action } from '@ember/object';
import { isPresent } from '@ember/utils';

export default class FileManagerImageThumbComponent extends Component {

  classNames = ['file-manager-image-thumb']

  @computed('file', 'selectedFile.id', 'selectedFileUrl', 'selectedFileSize')
  get selected() {
    const file = get(this, 'file');
    const selectedFile = get(this, 'selectedFile');
    const selectedFileUrl = get(this, 'selectedFileUrl');
    const selectedFileSize = get(this, 'selectedFileSize');
    
    if (file) {
      return isPresent(selectedFileUrl)
        ? get(file, 'filePath')(selectedFileSize) === selectedFileUrl
        : selectedFile
          ? get(file, 'id') === get(selectedFile, 'id')
          : false;
    }

    return false;
  }

  // must be override
  selectFile() {}
  deleteFile() {}

  mouseEnter() {
    set(this, 'hovering', true);
  }

  mouseLeave() {
    set(this, 'hovering', false);
  }

  @action
  _deleteFile() {
    this.deleteFile(get(this, 'file'));
  }

  @action
  _selectFile() {
    if (get(this, 'selected')) {
      set(this, 'file.selected', false);
    } else {
      set(this, 'file.selected', true);
    }

    this.selectFile(get(this, 'file'));
  }

}
