/* global pica: true */
import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { get, set, computed, action } from '@ember/object';
import { alias } from '@ember/object/computed';
import { later } from '@ember/runloop';
import { Promise, all } from 'rsvp';

export default class FileManagerComponent extends Component {
  
  classNames = ['file-manager']

  @service store
  @service fileManager
  @service currentUser

  @alias('fileManager.options')
  options

  constructor() {
    super(...arguments);
    this.sizes = [1920, 1280, 480];
  }

  _loadFiles() {
    get(this, 'store').findAll('file').then(result=> {
      set(this, 'files', result);
    });
  }

  @computed('files.@each.{name,type}', 'search')
  get filteredFiles() {
    const files = get(this, 'files') || [];
    const search = `${get(this, 'search') || ''}`.toLowerCase();

    if (!search) {
      return files;
    }

    return files.filter(file => {
      return `${get(file, 'name') || ''}`.toLowerCase().indexOf(search) !== -1
        || `${get(file, 'type') || ''}`.toLowerCase().indexOf(search) !== -1;
    });
  }

  loadFile(file) {
    return new Promise((resolve, reject) => {
      const fr = new FileReader();
      fr.onload = () => {
        const img = document.createElement('img');
        img.onload = () => resolve(img);
        img.src = fr.result;
      };
      fr.onerror = error => reject(error);
      fr.readAsDataURL(file);
    });
  }
  
  resizeImage(image, biggerSide) {
    return new Promise(resolve => {
      const w = image.width;
      const h = image.height;
      let resizeRatio;

      if (w > h) {
        resizeRatio = biggerSide / w < 1 ? biggerSide / w : 1;
      } else {
        resizeRatio = biggerSide / h < 1 ? biggerSide / h : 1;
      }

      const cw = Math.round(w * resizeRatio);
      const ch = Math.round(h * resizeRatio);

      const canvas = document.createElement('canvas');
      canvas.width = cw;
      canvas.height = ch;

      const success = async(result) => {
        const blob = await pica().toBlob(result, 'image/jpeg', 0.7);
        resolve({
          size: biggerSide,
          resolution: {
            w: cw,
            h: ch
          },
          file: blob
        });
      }

      pica().resize(image, canvas, {
        unsharpAmount: 80,
        unsharpRadius: 0.6,
        unsharpThreshold: 2
      }).then(success);
    });
  }

  generateResized(file) {
    const resize = this.sizes.map(size=> {
      return this.resizeImage(file, size);
    });

    return all(resize).then(results=> {
      return results;
    });
  }

  createFile(file, blobs) {
    let fileData = {
      is_image: false
    };

    if (blobs) {
      fileData.resolutions = {};
      fileData.is_image = true;
      blobs.forEach(blob=> {
        fileData.resolutions[blob.size] = {
          resolution: blob.resolution,
          size: blob.file.size
        }
      });
    }

    if (get(this, 'dominantColor')) {
      fileData.dominant_color = get(this, 'dominantColor');
    }

    return new Promise((resolve, reject) => {
      get(this, 'store').createRecord('file', {
        name: file.name,
        size: file.size,
        type: file.name.split('.').pop(),
        file_data: fileData,
        user: get(this, 'currentUser.user')
      }).save().then(result=> {
        set(this, 'dominantColor', false);
        resolve(result);
      }).catch(result=> {
        reject(result);
      });
    });
  }

  uploadFile(file, s3, dbFile, size) {
    return new Promise((resolve, reject) => {
      const fd = new FormData();
      fd.append('key', get(s3, 'key'));
      fd.append('acl', get(s3, 'acl'));
      fd.append('success_action_status', get(s3, 'success-action-status'));
      fd.append('x-amz-server-side-encryption', 'AES256');
      fd.append('X-Amz-Credential', get(s3, 'x-amz-credential'));
      fd.append('x-Amz-Algorithm', 'AWS4-HMAC-SHA256');
      fd.append('X-Amz-Date', get(s3, 'x-amz-date'));
      fd.append('policy', get(s3, 'policy'));
      fd.append('X-Amz-Signature', get(s3, 'signature'));
      fd.append('file', file);

      const xhr = new XMLHttpRequest();

      xhr.open('POST', get(s3, 'post-url'), true);
      // xhr.setRequestHeader('x-amz-content-sha256', get(s3, 'signature'));

      xhr.onreadystatechange = function() {
        if (xhr.readyState === xhr.DONE) {
          if (xhr.status === 201) {
            if (get(dbFile, 'file_data.is-image')) {
              set(dbFile, `file_data.resolutions.${size}.path`, get(s3, 'file_url'));
              set(dbFile, `file_data.resolutions.${size}.s3_path`, get(s3, 's3_path'));
            } else {
              set(dbFile, 'path', get(s3, 'file-url'));
              set(dbFile, 's3_path', get(s3, 's3-path'));
            }
            resolve(xhr);
          } else {
            reject();
          }
        }
      };

      xhr.send(fd);
    });
  }
  
  markFileUploaded(file/*, isImage*/) {
    const filePath = file && get(file, 'filePath')(480);
    
    const loadImage = () => {
      later(() => {
        const img = document.createElement('img');

        img.onload = function() {
          set(file, 'uploading', false);
          set(file, 'uploaded', true);
        }

        img.onerror = () => {
          loadImage();
        }

        img.src = filePath;
      }, 1000);
    }

    file && file.save()
      .then(loadImage);
  }

  @action
  upload(ev) {
    const file = ev.target.files[0];
    const extension = file.name.split('.').pop().toLowerCase();

    if (extension === 'jpg' || extension === 'png') {
      const onUploadImage = result => {
        this.markFileUploaded(result, true);
      };

      const onCreateImage = (blobs, result) => {
        set(result, 'uploading', true);

        const filesDataSets = blobs.map(blob => {
          return this.uploadFile(blob.file, result.signed_requests[blob.size], result, blob.size);
        });

        all(filesDataSets)
          .then(onUploadImage.bind(this, result));
      };

      const onLoadFile = async(result) => {
        const blobs = await this.generateResized(result);
        this.createFile(file, blobs)
          .then(onCreateImage.bind(this, blobs));
      };

      get(this, 'loadFile')(file)
        .then(onLoadFile);
    }
    else {
      const onUploadFile = result => {
        this.markFileUploaded(result);
      };

      const onCreateFile = result => {
        set(result, 'uploading', true);
        this.uploadFile(file, result.s3, result)
          .then(onUploadFile.bind(this, result));
      };

      this.createFile(file)
        .then(onCreateFile);
    }
  }
  
  dataURItoBlob(dataURI) {
      const byteString = atob(dataURI.split(',')[1]);
      const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
      }

      return new Blob([ab], {type: mimeString});
  }

  srcToFile(src, fileName, mimeType){
      return (fetch(src)
          .then(function(res){return res.arrayBuffer();})
          .then(function(buf){return new File([buf], fileName, {type:mimeType});})
      );
  }

  uploadImageMobile(dataURL) {
    const blob = this.dataURItoBlob(`data:image/jpeg;base64,${dataURL}`);
    blob.name = 'Mobile Upload.jpg';
    this.send('upload', {
      target: {
        files: [blob]
      }
    });
  }

  @action
  deleteFile(file) {
    set(file, 'deleting', true);

    const onError = () => {
      set(file, 'deleting', false);
      alert('Eror removing file');
    };

    file.deleteRecord();
    file.save().catch(onError);
  }

  @action
  selectFile(file) {
    const onSelect = get(this, 'options.onSelect');
    onSelect && onSelect(file);
    this.send('closeFileManager');
  }

  @action
  openFileManager() {
    const fileManager = get(this, 'fileManager');
    const options = get(this, 'options');
    get(fileManager, 'open').call(fileManager, options);
  }

  @action
  closeFileManager() {
    const fileManager = get(this, 'fileManager');
    get(fileManager, 'close').call(fileManager);
  }

}
