import Component from '@ember/component';
import { get, computed, action } from '@ember/object';
import { htmlSafe } from '@ember/string';
import ENV from 'seeds/config/environment';

export default class FileManagerImageWidgetComponent extends Component {

  classNames = ['file-manager-image-widget']

  rootURL = ENV.rootURL

  @computed('width', 'height', 'size', 'image.{isImage,is_ready}')
  get imageCSS() {
    const width = get(this, 'width') || 'auto';
    const height = get(this, 'height') || 'auto';
    const image = get(this, 'image');

    if (get(image, 'isImage') && get(image, 'is_ready')) {
      const imageURL = get(image, 'filePath')(get(this, 'size'));
      const color = get(image, 'file_data.dominant-color');
      let cssColor = "transparent";

      if (color && color.length === 3) {
        cssColor = `rgb(${color[0]}, ${color[1]}, ${color[2]})`;
      }

      return htmlSafe(`background-color: ${cssColor};width: ${width}; height: ${height};background-image:url(${imageURL})`);
    }
    else {
      return htmlSafe(`background-color: #fff;width: ${width}; height: ${height};background-image:url(${ENV.rootURL}assets/images/file-icon.png)`);
    }
  }

  @action
  _select() {
    const callback = get(this, 'onSelect');
    callback && callback(get(this, 'image'));
  }

}
