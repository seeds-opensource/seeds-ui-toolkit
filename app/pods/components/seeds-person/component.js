import Component from '@ember/component';
import { action, get, set, computed, setProperties } from '@ember/object';
import { inject as service } from '@ember/service';
import config from 'seeds/config/environment';
import { htmlSafe } from '@ember/template';

export default class SeedsPersonComponent extends Component.extend() {

  @service appState;
  @service seedsComponent;
  @service peopleManager;
  @service loadingOverlay;
  @service ajaxPassport;

  rootURL = config.rootURL;

  classNames = ['column'];

  likesCount = 0
  dislikesCount = 0
  commentsCount = 0
  editsCount = 0
  sharedCount = 0

  showModal = false;

  products = [];

  matchingCriteria = [];

  // Followin are default engine routes
  offeringRoute    = 'all.view.view-offering';
  viewProfileRoute = 'all.view';


  @computed('profileImage')
  get bgProfileImage() {
    const file = get(this, 'profileImage');
    return file ? htmlSafe( `background-image: url("${file}");` ) : "";
  }

  @computed('roles.[]')
  get topThreeProfileTag(){
    let tags = get(this, 'roles') || [];
    return tags.length > 3 ? tags.slice(0, 3) : tags;
  }


  @computed('roles.[]')
  get isRoleMultiple(){
    let tags = get(this, 'roles') || [];
    return tags.length > 1;
  }

  @computed('skill.[]')
  get isSkillMultiple(){
    let tags = get(this, 'skill') || [];
    return tags.length > 1;
  }

  @computed('interest.[]')
  get isInterestMultiple(){
    let tags = get(this, 'interest') || [];
    return tags.length > 1;    
  }

  @computed('type')
  get labels(){
    let type = get(this, 'type') || "";
    if( type === 'organisation' ){
      return {
        community: 'Community',
        contribution: 'Contribution',    
        reputation: 'Biosphere',    
        planted: 'Planted', // This will not be used    
        transaction: 'Economy', 
        offering: 'Direct Offering',
        story: 'Mission' 
      }
    }

    // community_score
    return {
      contribution: 'Contribution',    
      community: 'Community',    
      reputation: 'Reputation',    
      planted: 'Planted',         
      transaction: 'Transaction',
      offering: 'Offering',
      story: 'Profile Story'  
    }
  }

  @action
  singleView(){
    return get(this, 'viewPerson') ? get(this, 'viewPerson')(get(this, 'account')) : "";
  }

  showOverLay(){
    get(this, 'loadingOverlay').startLoading(); 
  }
  hideOverLay(){
    get(this, 'loadingOverlay').endLoading();
  }

  // Reset the variable configuration
  initScreenInfo(){
    setProperties(this, {
      product: []
    });
  }

  didReceiveAttrs(){
    this.initScreenInfo();
    this.getScreenInfo();
  }


  // TODO: For now using 50 need to make pagination working here
  async getProducts(page = 1, perPage = 50){
   
    let account = get(this, 'account');
    if( !account){
      return false;
    }
    return await this.ajaxPassport.request(`/products`,{
      data : {
        page,
        per_page: perPage,
        account_name: get(this, 'account')
      }
    });
  }


  //
  // Fetch Person details
  async getScreenInfo(){
    try{

      this.showOverLay();

      let pM = get(this, 'peopleManager');

      set(this, 'state', 'init');
      let model = get(this, 'model') || {};

      // In case account is passed then fetch the user from API
      if( get(this, 'account') ){
        model = await pM.getSinglePerson( get(this, 'account') );
        // Trying server call again, to make sure user doens't exists
        if( typeof(model.account) === 'undefined' ){
          model = await pM.getSinglePerson( get(this, 'account'), false );
        }
      }

      // TODO: Change the component to directly refer these var
      let usedKeys = {
        'title' : 'nickname',
        'account' : 'account',
        'profileImage' : 'image',
        'nickname' : 'nickname',
        'interest' : 'interests',
        'roles' : 'roles',
        'skill' : 'skills',
        'status' : 'status',
        'story' : 'story',
        'type' : 'type',
        'match' : 'match',
        'contribution' : 'contribution_score',
        'reputation' : 'reputation_score',
        'planted' : 'planted_score',
        'transaction' : 'transactions_score',
        // This are going to coming from backend later
        'likesCount' : 'likes',
        'dislikesCount' : 'dislikes',
        'commentsCount' : 'comments',
        'editsCount' : 'edits',
        'sharedCount' : 'shared',
      };
      for( let key in usedKeys){
        if( typeof( model[ usedKeys[key] ] ) !== 'undefined'){
          set(this, key, model[ usedKeys[key] ] );
        }
      }

      // Fetching user information
      if( get(this, 'showModal' ) === false ){
        set(this, 'state', 'finished');
        return;
      }
      
      let product = await this.getProducts();
      if( product && product.data ){
        set( this, 'products', product.data );
      }
      set(this, 'state', 'finished');
    }catch{
      set(this, 'state', 'error');
      // TODO: Improve error handling
      alert('Error while getting profile Information');
    }finally{
      this.hideOverLay();
    }

  }


  @action
  openModal() {
    get(this, 'appState').hideTopBar();
    get(this, 'appState').hideBottomBar();
    set(this, 'showModal', true);
  }

  @action
  closeModal() {


    // In case onClose is not passed then don't hide the model
    // Since it show's the card before going back
    if( get(this, 'onClose') ){

      get(this, 'appState').showTopBar();
      get(this, 'appState').showBottomBar();
      set(this, 'seedsComponent.showComponent', false);

      set(this, 'showModal', false);
      get(this, 'onClose')();
      return;
    }
    window.history.back();
  }

}
