import Component from '@ember/component';
import { get, set, computed, action }  from '@ember/object';

export default class FormRadioComponent extends Component {

  classNames = ['field', 'form-radio']

  classNameBindings = ['disabled']

  @computed('elementId')
  get radioId() {
    return `${this.elementId}-radio`;
  }

  @action
  onChange(value) {
    if (get(this, 'disabled')) {
      return;
    }

    const callback = get(this, 'on-change');
    if (callback) {
      return callback(value);
    }

    set(this, 'groupValue', value);
  }

}
