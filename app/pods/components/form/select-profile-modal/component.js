import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { action, get, set } from '@ember/object';

export default class SelectProfileModal extends Component {
  @service blockchainManager;

  constructor() {
    super(...arguments);
    const user = get(this, 'blockchainManager.currentBlockchainAccountName');
    get(this, 'blockchainManager').getProfile(user)
      .then(result=>
        set(this, 'displayName', result ? result.nickname : user));
  }

  @action
  openModal() {
    set(this, 'showModal', true);
  }

  @action
  closeModal() {
    set(this, 'showModal', false);
  }

  @action
  selectUser(user, balance, nickname) {
    set(this, 'selectedUser', user);
    set(this, 'balance', balance);
    set(this, 'displayName', nickname ? nickname : user)
    this.send('closeModal');
  }
}