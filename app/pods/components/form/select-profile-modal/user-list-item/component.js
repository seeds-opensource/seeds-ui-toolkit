/* eslint ember/no-incorrect-calls-with-inline-anonymous-functions: off */
import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { action, get, set } from '@ember/object';
import { run } from '@ember/runloop';

export default class SelectProfileULI extends Component {
  @service blockchainManager;
  
  constructor() {
    super(...arguments);
    
    run.scheduleOnce('afterRender', ()=> {
      get(this, 'blockchainManager').getBalance(get(this, 'profile.account_name'))
        .then(balance=>
          set(this, 'balance', balance));
    });
  }

  @action
  selectUser(user, balance, nickname) {
    if(get(this, 'onClick')) {
      this.onClick(user, balance, nickname);
    }
  }
}