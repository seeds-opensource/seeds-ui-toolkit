import Component from '@ember/component';
import { get, computed } from '@ember/object';
import { isBlank } from '@ember/utils';

export default class FormInput extends Component {

  textCentered = true

  /**
   * Errors can be a array or a string
   * The array values can be a string or an object containing 'message' attribute
   * We treat the received value to always result in a array of objects
   * 
   * errors='Test error'
   * Result: [{ message: 'Test error' }]
   * 
   * errors=['test 1', 'test 2', 'test 3']
   * Result: [{ message: 'test 1' }, { message: 'test 2' }, { message: 'test 3' }]
   * 
   * errors=[{ title: 'title 1', message: 'test 1' }, { title: 'title 2', message: 'test 2' }, { title: 'title 3', message: 'test 3' }]
   * Result: [{ message: 'test 1' }, { message: 'test 2' }, { message: 'test 3' }]
   */
  @computed('errors', 'errors.@each.message')
  get errorsList() {
    const errors = get(this, 'errors');

    if (isBlank(errors)) {
      return [];
    }

    return [].concat(errors).map(error => {
      if (typeof error === 'string') {
        return { message: error };
      }
      return { message: get(error, 'message') };
    });
  }

}