import Component from '@ember/component';
import { get, set, action, computed } from '@ember/object';
import { htmlSafe } from '@ember/string';
import ENV from 'seeds/config/environment';
import { inject as service } from '@ember/service';

export default class PersonBarComponent extends Component.extend() {

  classNames = ['person-bar-container', 'text', 'is-small']

  @service spamPopover;
  rootURL = ENV.rootURL

  disableLike = false
  disableDislike = false
  disableComment = false
  disableEdit = false
  disableShare = false

  showLike = true
  showDislike = true
  showComment = true
  showEdit = true
  showShare = true

  likesCount = 0
  dislikesCount = 0
  commentsCount = 0
  editsCount = 0
  sharedCount = 0

  spamOption = false

  @computed()
  get likeCss() {
    return htmlSafe(`background-image: url('${this.rootURL}assets/cooperation/triangle-up.svg')`)
  }

  @computed()
  get dislikeCss() {
    return htmlSafe(`background-image: url('${this.rootURL}assets/cooperation/triangle-down.svg')`)
  }

  @action
  onIconClick(type) {
    const callback = get(this, `on-${type}`);
    if(type === 'dislike' && get(this, 'disableDislike') === false ){
      set(this, 'spamPopover.show', true);
      set(this, 'spamPopover.callback', callback);
      return;
    }
    callback && callback( get(this, type)  );
  }

}
