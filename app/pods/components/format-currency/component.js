import Component from '@ember/component';
import { getProperties, computed } from '@ember/object';
import { and, or, equal } from '@ember/object/computed';

import { moneyQuantity, moneyCurrency } from  '../../utils/macros';
import { DEFAULT_CURRENCY } from '../../utils/currency';

export default class FormatCurrencyComponent extends Component {
  classNames = ['format-currency'];

  quantity;
  currency;
  value;
  hideCurrencyLabel = false;
  useCurrencyIcons = true;

  defaultQuantity = 0;
  defaultCurrency = DEFAULT_CURRENCY;
  defaultValue = `${this.defaultQuantity} ${this.defaultCurrency}`;

  minimumFractionDigits = 2;

  @or('value', 'defaultValue') assumedValue;
  @moneyQuantity('assumedValue') assumedValueQuantity;
  @moneyCurrency('assumedValue') assumedValueCurrency;
  @or('quantity', 'assumedValueQuantity', 'defaultQuantity') assumedQuantity;
  @or('currency', 'assumedValueCurrency', 'defaultCurrency') assumedCurrency;

  @equal('assumedCurrency', DEFAULT_CURRENCY) isDefaultCurrency;
  @and('isDefaultCurrency', 'useCurrencyIcons') showDefaultCurrencyIcon;

  @computed('assumedQuantity', 'minimumFractionDigits')
  get formattedQuantity() {
    const {
      assumedQuantity,
      minimumFractionDigits
    } = getProperties(this,
      'assumedQuantity',
      'minimumFractionDigits'
    );
    const truncatedQuantity = Math.trunc(assumedQuantity * 100) / 100;
    return truncatedQuantity.toLocaleString('en-US', {
      minimumFractionDigits
    });
  }
}
