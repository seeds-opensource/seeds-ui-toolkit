import Component from '@ember/component';
import { get, set, action, computed } from '@ember/object';
import { isPresent } from '@ember/utils';

export default class SwitchComponent extends Component {

  classNames = ['field', 'switch-container']

  checked = false
  reverse = false

  @computed('elementId')
  get inputId() {
    return `${this.elementId}-checkbox`;
  }

  /**
   * Trigerred when the switch's value is changed
   * The 'checked' property must always be muted or the switch won't show as checked
   */
  @action
  onChange() {
    if (!this._triggerCallback('on-change', arguments)) {
      set(this, 'checked', !get(this, 'checked'));
    }
  }

  _triggerCallback(name, params) {
    const callback = get(this, name);
    callback && callback(...params);

    return isPresent(callback);
  }
  
}
